OUTDIR = ${GOPATH}/bin/backend
VERSION = 0.1

# Compile for macOS 
normal: clean
	env CGO_ENABLED=1 go build --tags "darwin" -o $(OUTDIR)/backend_mac
	cp conf.yaml $(OUTDIR)/.
	
# Generate code for linux
linux: clean
	env GOOS=linux GOARCH=amd64 go build -o $(OUTDIR)/backend_linux
	cp conf.yaml $(OUTDIR)/.
	zip -9 $(OUTDIR)/backend_linux_v$(VERSION).zip $(OUTDIR)/backend_linux $(OUTDIR)/conf.yaml

# Generate code for windows
windows: clean
	env GOOS=windows GOARCH=amd64 go build -o $(OUTDIR)/backend_amd64.exe
	cp conf.yaml $(OUTDIR)/.
	zip -9 $(OUTDIR)/backend_win_v$(VERSION).zip $(OUTDIR)/backend_amd64.exe $(OUTDIR)/conf.yaml

# Clean all old files
clean:
	rm -f $(OUTDIR)/backe*
	rm -f $(OUTDIR)/*.exe
