package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/nu7hatch/gouuid"
	"github.com/rs/zerolog/log"
	"net/http"
	"sort"
	"strconv"
	"strings"
)

var (
	strContentType     = []byte("Content-Type")
	strApplicationJSON = []byte("application/json")
)

// Handle hello calls returning uuid
func helloHandler(w http.ResponseWriter, r *http.Request) {

	u, err := uuid.NewV4()
	if err != nil {
		fmt.Fprint(w, "Error getting UUID", err.Error())
	}
	fmt.Fprintf(w, "%s", u.String())

}

// Handle customerAccounts path
// Return all the accounts.
// Receives no parameters.
func customerAccountsHandler(db *sql.DB) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		rows, err := db.Query("select * from customer_account")
		if err != nil {
			http.Error(w, "Error on query DB", http.StatusBadRequest)
			log.Error().Msgf("Error en response: %s", err)
			return
		}
		defer rows.Close()

		myData := CustomerAccounts{}

		for rows.Next() {
			var ca CustomerAccount

			err = rows.Scan(&ca.Id, &ca.Name, &ca.Username)
			if err != nil {
				log.Error().Msgf("Error: %s", err)
			}

			myData = append(myData, ca)
			//fmt.Println(id, name, username)
		}

		err = rows.Err()
		if err != nil {
			http.Error(w, "Error retrieveing data from DB", http.StatusInternalServerError)
			log.Error().Msgf("%s", err)
			return
		}

		if err := json.NewEncoder(w).Encode(myData); err != nil {
			log.Error().Msgf("%s", err.Error())
			http.Error(w, "Error preparing JSON", http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		log.Debug().Msgf("Returning: %v\n", myData)
	}

}

// Return data of requested account.
// Receives accountId.
func customerAccountHandler(db *sql.DB) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		if len(r.FormValue("accountId")) == 0 {
			http.Error(w, "Parameter accountId not present", http.StatusBadRequest)
			log.Error().Msg("Parameter accountId not present")
			return
		}

		rows, err := db.Query("select * from customer_account where id = " + r.FormValue("accountId"))
		if err != nil {
			http.Error(w, "Error from DB", http.StatusBadRequest)
			log.Error().Msgf("Error from DB %s", err.Error())
			return
		}
		defer rows.Close()

		var ca CustomerAccount

		for rows.Next() {

			err = rows.Scan(&ca.Id, &ca.Name, &ca.Username)
			if err != nil {
				log.Error().Msgf("Error: %s", err)
			}

		}

		err = rows.Err()
		if err != nil {
			http.Error(w, "Error from queries", http.StatusInternalServerError)
			log.Error().Msgf("%s", err)
			return
		}

		if err := json.NewEncoder(w).Encode(ca); err != nil {
			log.Error().Msgf("%s", err.Error())
			http.Error(w, "Error encoding JSON", http.StatusInternalServerError)
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)

		log.Debug().Msgf("Returning: %v\n", ca)
	}

}

// Retrieves all the customer account details
// Receives the following parameters
// 		accountId - a number with the account
// Returns details of such account
func customerAccountDetailHandler(db *sql.DB) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		var qryStr strings.Builder

		qryStr.WriteString("SELECT a.ID as id, a.NAME as name, a.USERNAME as username, ")
		qryStr.WriteString("count(m.id) as movements, SUM(m.AMOUNT) as totalAmount ")
		qryStr.WriteString("FROM CUSTOMER_ACCOUNT a, CUSTOMER_ACCOUNT_MOVEMENTS m ")
		qryStr.WriteString("WHERE a.ID = ")
		qryStr.WriteString(fmt.Sprintf("%s", r.FormValue("accountId")))
		qryStr.WriteString(" AND a.ID = m.CUSTOMER_ACCOUNT_ID ")
		qryStr.WriteString("GROUP BY a.ID, a.NAME, a.USERNAME")

		log.Debug().Msgf("Query str: %s", qryStr.String())

		rows, err := db.Query(qryStr.String())
		if err != nil {
			http.Error(w, "Error on query to DB", http.StatusBadRequest)
			log.Error().Msgf("Error en response: %s", err)
			return
		}
		defer rows.Close()

		var ca CustomerAccountDetails

		for rows.Next() {

			err = rows.Scan(&ca.Id, &ca.Name, &ca.Username, &ca.Movements, &ca.TotalAmount)
			if err != nil {
				log.Error().Msgf("Error: %s", err)
			}
		}

		if err := json.NewEncoder(w).Encode(ca); err != nil {
			log.Error().Msgf("%s", err.Error())
			http.Error(w, "Error preparing JSON", http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)

		log.Debug().Msgf("Returning: %v\n", ca)
	}

}

// Retrieves all the customer account movements
// Received parameters:
//		accountId - number with the account
// 		sort	  - true or false or not present
//		asc		  - true or false or not present
// Returns the list of movements sorted if requested
func customerAccountMovementsHandler(db *sql.DB) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		log.Debug().Msgf("AccountId: %s, Sort: %s, Asc: %s\n\n",
			r.FormValue("accountId"),
			r.FormValue("sort"),
			r.FormValue("asc"))

		// Check if accountId was received
		if len(r.FormValue("accountId")) == 0 {
			http.Error(w, "Argument accountId not present", http.StatusBadRequest)
			log.Error().Msg("Argument accountId not present")
			return
		}

		// Create the query string
		var qryStr strings.Builder
		qryStr.WriteString("SELECT m.id, m.movement_date, m.amount, m.concept, m.customer_account_id")
		qryStr.WriteString(" FROM CUSTOMER_ACCOUNT_MOVEMENTS m ")
		qryStr.WriteString(" WHERE m.CUSTOMER_ACCOUNT_ID = ")
		qryStr.WriteString(fmt.Sprintf("%s", r.FormValue("accountId")))

		log.Debug().Msgf("Query str: %s", qryStr.String())

		// Query the DB
		rows, err := db.Query(qryStr.String())
		if err != nil {
			http.Error(w, "No content", http.StatusNoContent)
			log.Error().Msgf("Error en response: %s", err)
			return
		}
		defer rows.Close()

		// Start to get the data
		myData := CustomerAccountMovements{}

		for rows.Next() {
			var cam CustomerAccountMovement

			err = rows.Scan(&cam.Id, &cam.MovementDate, &cam.Amount, &cam.Concept, &cam.CustomerAccountId)
			if err != nil {
				log.Error().Msgf("Error: %s", err)
			}

			myData = append(myData, cam)
		}

		// Get any error occured during rows iteration
		err = rows.Err()
		if err != nil {
			http.Error(w, "No content", http.StatusNoContent)
			log.Error().Msgf("%s", err)
			return
		}

		// If no data on slice, return no data to caller.
		if len(myData) == 0 {
			http.Error(w, "No content", http.StatusNoContent)
			log.Error().Msg("No content")
			return
		}

		// Check if sort be performed as per received parameter
		if len(r.FormValue("sort")) > 0 {
			// if 1, shall sort
			if strings.Compare(string(r.FormValue("sort")), "true") == 0 {
				// Check is asc is present
				if len(r.FormValue("asc")) > 0 {
					// if 1, sort asc
					if strings.Compare(string(r.FormValue("sort")), "true") == 0 {
						sort.Slice(myData, func(i, j int) bool { return myData[i].Id < myData[j].Id })
					} else {
						// desc
						sort.Slice(myData, func(i, j int) bool { return myData[i].Id > myData[j].Id })
					}
				}
			} else {
				// Sort desc by default when asc was not received
				sort.Slice(myData, func(i, j int) bool { return myData[i].Id > myData[j].Id })
			}
		}

		// Encode the JSON response and check for errors.
		if err := json.NewEncoder(w).Encode(myData); err != nil {
			log.Error().Msgf("%s", err.Error())
			http.Error(w, "Error preparing JSON", http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)

		log.Debug().Msgf("Returning: %v\n", myData)

	}
}

// Retrieves all the customer account movements but will display just the top
// requested after sort them if required.
//		accountId 		- number with the account
// 		totalElements	- number of rows to show
//		asc		  		- true or false or not present
// Returns the list of movements sorted if requested
func customerAccountMovementsTopHandler(db *sql.DB) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		log.Debug().Msgf("AccountId: %s, Sort: %s, Asc: %s\n\n",
			r.FormValue("accountId"),
			r.FormValue("totalElements"),
			r.FormValue("asc"))

		// Check if accountId was received
		if len(r.FormValue("accountId")) == 0 {
			http.Error(w, "Argument accountId not present", http.StatusBadRequest)
			log.Error().Msg("Argument accountId not present")
			return
		}

		// Check if totalElements is present
		if len(r.FormValue("totalElements")) == 0 {
			http.Error(w, "Argument totalElements is not present", http.StatusBadRequest)
			log.Error().Msg("Argument totalElements is not present")
			return
		}

		// Create the query string
		var qryStr strings.Builder
		qryStr.WriteString("SELECT m.id, m.movement_date, m.amount, m.concept, m.customer_account_id")
		qryStr.WriteString(" FROM CUSTOMER_ACCOUNT_MOVEMENTS m ")
		qryStr.WriteString(" WHERE m.CUSTOMER_ACCOUNT_ID = ")
		qryStr.WriteString(fmt.Sprintf("%s", r.FormValue("accountId")))

		log.Debug().Msgf("Query str: %s", qryStr.String())

		// Query the DB
		rows, err := db.Query(qryStr.String())
		if err != nil {
			http.Error(w, "No content", http.StatusNoContent)
			log.Error().Msgf("Error en response: %s", err)
			return
		}
		defer rows.Close()

		// Start to get the data
		myData := CustomerAccountMovements{}

		for rows.Next() {
			var cam CustomerAccountMovement

			err = rows.Scan(&cam.Id, &cam.MovementDate, &cam.Amount, &cam.Concept, &cam.CustomerAccountId)
			if err != nil {
				log.Error().Msgf("Error: %s", err)
			}

			myData = append(myData, cam)
		}

		// Get any error occured during rows iteration
		err = rows.Err()
		if err != nil {
			http.Error(w, "No content", http.StatusNoContent)
			log.Error().Msgf("%s", err)
			return
		}

		// If no data on slice, return no data to caller.
		if len(myData) == 0 {
			http.Error(w, "No content", http.StatusNoContent)
			log.Error().Msg("No content")
			return
		}

		// Check if sort be performed as per received parameter
		if len(r.FormValue("asc")) > 0 {
			// if 1, shall sort
			if strings.Compare(string(r.FormValue("asc")), "true") == 0 {
				// if 1, sort asc
				if strings.Compare(string(r.FormValue("sort")), "true") == 0 {
					sort.Slice(myData, func(i, j int) bool { return myData[i].Id < myData[j].Id })
				}
			}

		}

		totalElements, err := strconv.Atoi(r.FormValue("totalElements"))
		if err != nil {
			http.Error(w, "Wrong parameters", http.StatusBadRequest)
			log.Error().Msgf("%s", err)
			return
		}

		top := CustomerAccountMovements{}
		for x := 0; x < totalElements; x++ {
			top = append(top, myData[x])
		}

		// Encode the JSON response and check for errors.
		if err := json.NewEncoder(w).Encode(top); err != nil {
			log.Error().Msgf("%s", err.Error())
			http.Error(w, "Error procesing JSON", http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)

		log.Debug().Msgf("Returning: %v\n", myData)

	}
}

// Retrieves all the customer account movements but will display just the top
// requested after sort them if required.
//		accountId 		- number with the account
// Returns the list of movements sorted if requested
func customerAccountMovementsBalanceHandler(db *sql.DB) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		log.Debug().Msgf("AccountId: %s, Sort: %s, Asc: %s\n\n",
			r.FormValue("accountId"))

		// Check if accountId was received
		if len(r.FormValue("accountId")) == 0 {
			http.Error(w, "Argument accountId not present", http.StatusBadRequest)
			log.Error().Msg("Argument accountId not present")
			return
		}

		customeraccountid, err := strconv.Atoi(r.FormValue("accountId"))
		if err != nil {
			http.Error(w, "Error procesing parameter accountID", http.StatusBadRequest)
			log.Error().Msgf("%s", err.Error())
			return
		}

		// Create the query string
		var qryStr strings.Builder
		qryStr.WriteString("SELECT m.amount, m.customer_account_id")
		qryStr.WriteString(" FROM CUSTOMER_ACCOUNT_MOVEMENTS m ")
		qryStr.WriteString(" WHERE m.CUSTOMER_ACCOUNT_ID = ")
		qryStr.WriteString(fmt.Sprintf("%s", r.FormValue("accountId")))

		log.Debug().Msgf("Query str: %s", qryStr.String())

		// Query the DB
		rows, err := db.Query(qryStr.String())
		if err != nil {
			http.Error(w, "No content", http.StatusNoContent)
			log.Error().Msgf("Error en response: %s", err)
			return
		}
		defer rows.Close()

		// Start to get the data
		var balanceAmount float32

		for rows.Next() {
			var cam CustomerAccountMovement

			err = rows.Scan(&cam.Amount, &cam.CustomerAccountId)
			if err != nil {
				log.Error().Msgf("Error: %s", err)
			}

			balanceAmount = balanceAmount + float32(cam.Amount)
		}

		// Get any error occured during rows iteration
		err = rows.Err()
		if err != nil {
			http.Error(w, "No content", http.StatusNoContent)
			log.Error().Msgf("%s", err)
			return
		}

		cab := CustomerAccountBalance{
			CustomerAccountId: customeraccountid,
			Balance:           balanceAmount,
		}

		// Encode the JSON response and check for errors.
		if err := json.NewEncoder(w).Encode(cab); err != nil {
			log.Error().Msgf("%s", err.Error())
			http.Error(w, "Error procesing JSON", http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)

		log.Debug().Msgf("Returning: %v\n", cab)

	}
}
