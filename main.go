package main

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/gorilla/mux"
	_ "github.com/mattn/go-sqlite3"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"net/http"

	//_ "net/http/pprof"
	"os"
	"os/signal"
	"time"
)

// Display start messages
func start() {

	fmt.Println("Starting server")

	// Start to read conf file
	fmt.Print("\n\n")
	fmt.Println("=============================================")
	fmt.Println("    Configuration checking - backend v0.1")
	fmt.Println("=============================================")

	// loading configuration
	viper.SetConfigName("conf")          // name of config file (without ext)
	viper.AddConfigPath(".")             // default path for conf file
	viper.SetDefault("port", ":9596")    // default port value
	viper.SetDefault("loglevel", "info") // default port value
	err := viper.ReadInConfig()          // Find and read the config file
	if err != nil {                      // Handle errors reading the config file
		fmt.Printf("Fatal error config file: %v \n", err)
		panic(err)
	}

	fmt.Println("-- Using port:       ", viper.GetString("port"))
	if len(viper.GetString("db")) == 0 {
		panic("No db parameter. Shuting down...")
	} else {
		fmt.Println("-- Using db:        ", viper.GetString("db"))
	}

	zerolog.TimeFieldFormat = time.RFC3339
	zerolog.TimestampFieldName = "@timestamp"
	switch viper.GetString("loglevel") {
	case "info":
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	case "debug":
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	case "disabled":
		zerolog.SetGlobalLevel(zerolog.Disabled)
	default:
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	fmt.Println("-- Log lever set to:    ", viper.GetString("loglevel"))

	fmt.Println("=============================================")

}

func main() {
	start()

	// CPU profiling by default
	//defer profile.Start().Stop()
	// Memory profiling
	//defer profile.Start(profile.MemProfile).Stop()

	// Trying to open a DB
	db, err := sql.Open("sqlite3", "./db/zerodb.db")
	if err != nil {
		log.Fatal().Msgf("%s", err)
	}
	defer db.Close()

	// ------------------------------------------
	// /customer/accounts					- OK
	// /customer/account					- OK
	// /customer/account/detail				- OK
	// /customer/account/movements			- OK
	// /customer/account/movements/top		- OK
	// /customer/account/movements/balance	- OK
	// ------------------------------------------

	r := mux.NewRouter()
	r.Path("/hello").HandlerFunc(helloHandler).Methods("GET")
	r.Path("/customer/accounts").Queries().HandlerFunc(
		customerAccountsHandler(db)).Methods("GET")
	r.Path("/customer/account").Queries().HandlerFunc(
		customerAccountHandler(db)).Methods("GET")
	r.Path("/customer/account/detail").Queries().HandlerFunc(
		customerAccountDetailHandler(db)).Methods("GET")
	r.Path("/customer/account/movements").Queries().HandlerFunc(
		customerAccountMovementsHandler(db)).Methods("GET")
	r.Path("/customer/account/movements/top").Queries().HandlerFunc(
		customerAccountMovementsTopHandler(db)).Methods("GET")
	r.Path("/customer/account/movements/balance").Queries().HandlerFunc(
		customerAccountMovementsBalanceHandler(db)).Methods("GET")

	srv := &http.Server{
		Addr:        viper.GetString("port"),
		Handler:     r,
		ReadTimeout: 10 * time.Second,
		//WriteTimeout:   10 * time.Second,
		//MaxHeaderBytes: 1 << 20,
	}

	// Lanuch server in a thread
	go func() {
		fmt.Println("Starting server...")
		log.Info().Msg("Starting server...")
		if err := srv.ListenAndServe(); err != nil {
			log.Panic().Msgf("%s", err)
		}
	}()

	// Process a graceful shutdown
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	// Block until we receive our signal.
	<-c

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	errShutdown := srv.Shutdown(ctx)
	if errShutdown != nil {
		panic(fmt.Sprintf("Error shutting down %s", errShutdown))
	}

	// Optionally, you could run srv.Shutdown in a goroutine and block on
	// <-ctx.Done() if your application should wait for other services
	// to finalize based on context cancellation.
	fmt.Print("\n\n")
	fmt.Println("shutting down")
	fmt.Println("Goddbye!....")
	os.Exit(0)

}
