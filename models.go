package main

// Defines a data type for CustomerAccount.
type CustomerAccount struct {
	Id       int    `json:"id"`
	Name     string `json:"name"`
	Username string `json:"username"`
}

type CustomerAccounts []CustomerAccount

type CustomerAccountDetails struct {
	Id          int     `json:"id"`
	Name        string  `json:"name"`
	Username    string  `json:"username"`
	Movements   float32 `json:"movements"`
	TotalAmount float32 `json:"totalamount"`
}

type CustomerAccountsDetails []CustomerAccountDetails

type CustomerAccountMovement struct {
	Id                int     `json:"id"`
	MovementDate      string  `json:"movementdate"`
	Amount            float32 `json:"amount"`
	Concept           string  `json:"concept"`
	CustomerAccountId int     `json:"customeraccountid"`
}

type CustomerAccountMovements []CustomerAccountMovement

type CustomerAccountBalance struct {
	CustomerAccountId int     `json:"customeraccountid"`
	Balance           float32 `json:"balance"`
}
